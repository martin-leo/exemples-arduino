# Montage d'un servo piloté par une photorésistance

## Définitions :

* servomoteur : un servomoteur est un moteur qui :
 * peut habituellement connaître sa position (angle)
 * peut recevoir l'ordre d'effectuer une rotation jusqu'à un angle donné
 * a une amplitude habituellement contrainte, de 0° à 270° par exemple.
 * il existe une exception : le servo continu qui :
  * n'est pas contraint en amplitude
  * n'est pas capable de connaître sa position
  * ne peut donc que de tourner (ou non) de manière continue dans un sens ou dans l'autre.
* photorésistance
 * résistance dont la résistance varie selon l'exposition à la lumière

## Composants nécéssaire

* un servomoteur
* une photorésistance
* une résistance 10Kohms
* une carte arduino uno
* une *breadboard*
* des câbles

## Image du Montage

![photographie du montage d'un servo piloté par une photorésistance](images/photoresistance-servomoteur.jpg)

> Une vidéo du montage est disponible dans ce dépôt

## Schéma

![schéma du montage d'un servo piloté par une photorésistance](images/photoresistance-vers-servomoteur_bb.png)

## Montage

### servo

Le servo dispose de trois câbles :
 * rouge : alimentation, à brancher au 5V de la carte
 * noir : masse, à brancher à la masse (gnd : ground)
 * blanc : commande, à brancher a un pin PWM (ici le 9)

### photorésistance

La photorésistance se branche en série avec la résistance de 10Kohms, entre le 5V et la masse de la carte.

Un câble situé entre la liaison photorésistance/résistance est relié à une entrée analogie que la carte (ici l'entrée analogique 0).

## Programme

Le programme effectue une boucle infinie. Une valeur précisée de luminosité permet de faire alterner la position du servomoteur entre une position haute et une position basse.
