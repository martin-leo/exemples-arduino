/*

  Servo piloté par une photorésistance
  
  Ce programme permet d'utiliser une photorésistance
  (une résistance qui varie selon la luminosité)
  pour piloter un servomoteur
  (un moteur qui peut effectuer une rotation entre un angle x et y
  et dont il est possible de choisir l'angle)

*/

/* on indique au programme de charger le code préécrit
   qui va nous permettre de piloter le servomoteur
*/
#include <Servo.h>

// on créé un objet Servo pour le pilotage du servo
Servo servomoteur_drapeau;

// numéro des pins
int pin_capteur_photosensible = 0;
int pin_servomoteur_drapeau = 9;

// variable pour l'enregistrement
// de la position du servomoteur (en degré)
int position_servomoteur;

// paramètres
int position_haute = 90;
int position_basse = 0;
// valeur limite de luminosité entre la position basse et haute
int niveau_luminosite_declenchement = 800;

void setup() {
  /* On définit ici la fonction setup
     qui est appelée la première par Arduino
     Cette fonction ne sera appelée qu'une fois
  */
  
  // on paramètre l'objet servo en lui indiquant le pin
  // ou le servo est branché
  servomoteur_drapeau.attach(pin_servomoteur_drapeau);
  // on le met en position basse
  servomoteur_drapeau.write(position_basse);
  // on enregistre sa position dans la variable dédiée
  position_servomoteur = position_basse;
  
  // on démarre une communication
  // (optionnel, si branché à un ordinateur en usb)
  // depuis l'Arduino vers l'ordinateur
  // à 9600 bps
  Serial.begin(9600);
}

void loop() {
  /* On définit ici la fonction loop
     qui sera appelée par Arduino en continu
  */
  // imprime le niveau de luminosité détecté
  // dans le moniteur série
   imprimer_valeur_photoresistance();
  
  // on récupère l'information niveau de lumière capté
  int niveau_lumiere = analogRead(pin_capteur_photosensible);
  
  
  // si le niveau de luminosité est en dessous de valeur limite
  // et pas déjà en position
  if (niveau_lumiere < niveau_luminosite_declenchement && position_servomoteur != position_haute) {
    
    // on commande au servomoteur de se mettre en position haute
    servomoteur_drapeau.write(position_haute);
    
    // on enregistre le fait qu'on a lancé la commande
    position_servomoteur = position_haute;
    
  // si le niveau de luminosité est au dessus de la valeur limite
  // et pas déjà en position
  } else if (niveau_lumiere >= niveau_luminosite_declenchement && position_servomoteur != position_basse){
    
    // on commande au servomoteur de se mettre en position basse
    servomoteur_drapeau.write(position_basse);
    
    // on enregistre le fait qu'on a lancé la commande
    position_servomoteur = position_basse;
  }
 
  // on met un délai de 250 millisecondes avant de réexécuter
  // le code car c'est bien suffisant !
  delay(250);
}

void imprimer_valeur_photoresistance() {
  /* Cette fonction imprime dans le moniteur série
     le niveau lumière passé en argument */
  Serial.print(analogRead(pin_capteur_photosensible));
  Serial.print("\n");
}
